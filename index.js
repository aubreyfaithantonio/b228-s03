// Activity:
// =================
// 		 Quiz
// =================
// 1. What is the blueprint where objects are created from?
// Class

// 2. What is the naming convention applied to classes?
// Should begin with capital letters 

// 3. What keyword do we use to create objects from a class?
// new

// 4. What is the technical term for creating an object from a class?
// Instantiation

// 5. What class method dictates HOW objects will be created from that class?
//Constructor

// ========================
//     Function Coding
// ========================

// 1. Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.

// class Student{

//     constructor(name, email, grades){
//         this.name = name;
//         this.email = email;
//         this.grade = (grades.every(grade => (grade >= 0 && grade <= 100 && typeof grade === "number")) && grades.length === 4)? grades : undefined;
//     }
// }


// 2. Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.

// let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
// let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
// let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
// let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);


// */

// PART 2

//======================
//        QUIZ
//======================

// 1. Should class methods be included in the class constructor?
// No

// 2. Can class methods be separated by commas?
// No

// 3. Can we update an object’s properties via dot notation?
// Yes

// 4. What do you call the methods used to regulate access to an object’s properties?
// Getter Setter

// 5. What does a method need to return in order for it to be chainable?
// this


//===========================
//     FUNCTION CODING
//===========================

// Modify the Student class to allow the willPass() and willPassWithHonors() methods to be chainable.

class Student{

    constructor(name, email, grades){
        this.name = name;
        this.email = email;
        this.grades = (grades.every(grade => (grade >= 0 && grade <= 100 && typeof grade === "number")) && grades.length === 4)? grades : undefined;
    }

    willPass() {
        this.passed = (this.gradeAve >= 85)
        return this
    }

    computeAve() {
        this.gradeAve = (this.grades.reduce((partialSum, a) => partialSum + a, 0))/this.grades.length
        return this
    }

    willPassWithHonors() {
        this.passedWithHonors = (this.gradeAve >= 90 && this.gradeAve <= 100)
        return this;
    }

    login() {
        console.log(`${this.email} has logged in`)
        return this
    }

    logout() {
        console.log(`${this.email} has logged out`)
        return this
    }
}